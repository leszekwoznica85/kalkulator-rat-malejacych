calc.component("resultView", {
  bindings: {loanMonths : "=" , loanAmount: "=",loanTemp : "="  },
  templateUrl: "../src/calc/resultView/resultView-view.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,
  controller() {
    this.round = (number)=>{
      return (number/100).toFixed(2)
    }

  },
});
