calc.component("inputForm", {
  bindings: { loanMonths: "=", loanAmount: "=", loanTemp : "=", loanInterest: "="  },
  templateUrl: "../src/calc/inputForm/inputForm-view.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,
  controller($scope) {
    this.onInit = () => {
      console.log("inputForm");
    };
    this.loan = []
    this.loan = [{
      amountOfCapital: 0, //kwota kapitału
      installment: 0, //rata
      interest: 10, //odsetki
      interestPart: 10, //część odsetkowa
      capitalPart: 0, //część kapitwałowa
      installmentNumber:  0, //numer raty
      }]

    this.calculateLoan = () => {
      console.log(this);
      this.i = 1;
      this.a = 0;
      this.loan[0].loanAmount = parseInt(this.loanAmount)*100;
      this.loan[0].loanMonths = parseInt(this.loanMonths)
      debugger
      this.loan[0].interest = parseInt(this.loanInterest*100)/100
      this.loan[0].amountOfCapital = this.loan[0].loanAmount
      while ( this.a < this.loan[0].loanMonths) {

        //console.log(this.loan[a].amountOfCapital)
        //console.log(this.loan[0].loanMonths)
        this.capitalPart = this.loan[0].loanAmount / this.loan[0].loanMonths;
        //console.log(capitalPart);
        this.interestPart =
          (this.loan[this.a].amountOfCapital * (this.loan[0].interest / 100)) /
          this.loan[0].loanMonths;
        //console.log(this.interestPart);
        this.installment = this.capitalPart + this.interestPart;
        //console.log(a);

        //this.loan2.push({amountOfCapital:amountOfCapital,rata:rata,interestPart:interestPart,capitalPart:capitalPart,numerRaty:i})
        this.amountOfCapital = this.loan[this.a].amountOfCapital - this.capitalPart;

        this.loan.push({
          amountOfCapital: this.amountOfCapital,
          installment: this.installment,
          interestPart: this.interestPart, 
          capitalPart: this.capitalPart,
          installmentNumber:  this.i,
        });

        this.i++;
        this.a++;
      }
      //this.loan2[0].amountOfCapital = this.loan[0].loanAmount gdy chcemy by wyglądało tak jak w linku
      //console.log(this.loan)
      this.loan = this.loan.slice(1, this.loan.length);
      console.log(this.loan)
      this.loanTemp = {...this.loan}
    };
  },
});
console.log("file calcComponent");
